import todosService from './todosService.js';

function createTodoList() {
  // TODOs nach Status erledigt / nicht erledigt sortieren
  todos.sort((todo1, todo2) => {
    if (todo1.isDone && !todo2.isDone) {
      return 1;
    }
    if (!todo1.isDone && todo2.isDone) {
      return -1;
    }
    return 0;
  });
  const elTodoList = document.getElementById('todo-list');
  // Bisherige Liste löschen
  elTodoList.innerHTML = '';
  // TODOs durchgehen und alle in Liste einfügen
  for (let i = 0; i < todos.length; i++) {
    const todo = todos[i];
    const elItem = createItemWithCheckbox(todo);
    elTodoList.appendChild(elItem);
  }
}

function createItemWithCheckbox(todo) {
  // Listen Item erstellen
  const elLi = document.createElement('li');
  // Checkbox erstellen
  const elCheck = document.createElement('input');
  elCheck.setAttribute('type', 'checkbox');
  const todoId = 'check-' + todo.id;
  elCheck.setAttribute('id', todoId);
  // Event-Listener für Checkbox hinzufügen
  elCheck.addEventListener('change', () => {
    // Status erledigt / nicht erledigt in Backend updaten
    todosService
      .update(todo.id, {
        description: todo.description,
        isDone: elCheck.checked,
      })
      .then((updatedTodo) => {
        // Status erledigt / nicht erledigt in Frontend updaten
        todo.isDone = updatedTodo.isDone;
        // Todo Liste in DOM neu erzeugen
        createTodoList();
      });
  });
  // Checkbox Item hinzufügen
  elLi.appendChild(elCheck);
  // Label erstellen
  const elLabel = document.createElement('label');
  elLabel.setAttribute('for', todoId);
  // Styling für erledigte TODOs richtig setzen
  if (todo.isDone) {
    elCheck.checked = true;
    elLabel.classList.add('done');
  }
  // Todo als Inhalt Label hinzufügen
  elLabel.textContent = todo.description;
  // Label Item hinzufügen
  elLi.appendChild(elLabel);
  return elLi;
}

function addNewTodo() {
  // eingegebenes Todo auslesen
  const elTodo = document.getElementById('todo');
  const todo = elTodo.value;
  if (todo) {
    // neues Todo Backend hinzufügen
    todosService
      .create({ description: todo, isDone: false })
      .then((newTodo) => {
        // neues Todo Frontend hinzufügen
        todos.push(newTodo);
        createTodoList();
      });
  }
  elTodo.value = '';
}

const elAddBtn = document.getElementById('add-new-todo');
elAddBtn.addEventListener('click', addNewTodo);

let todos = [];
// Zu Beginn alle Todos vom Backend laden
todosService.getAll().then((data) => {
  // Geladene TODOs in Frontend als Array von
  // Objekten speichern
  for (let i = 0; i < data.length; i++) {
    const todo = data[i];
    todos.push(todo);
  }
  createTodoList();
});
